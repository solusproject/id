import { Link } from "react-router-dom";

export const Sidebar = (id) => {
    return (
        <aside class="bg-white py-5 w-1/6 float-right">
            <div class="mr-50 rounded rounded-r-3xl pl-6 py-3 font-semibold">
                <Link to={`/${id.id}`} class="text-blue-500 text-sm font-semibold flex items-center  focus:outline-none">
                    About User
                </Link>
            </div>
            <div class="mr-50 rounded rounded-r-3xl pl-6 py-3 font-semibold">
                <Link to={`/${id.id}/wall`} class="text-blue-500 text-sm font-semibold flex items-center  focus:outline-none">
                    Wall
                </Link>
            </div>
            <div class="mr-50 rounded rounded-r-3xl pl-6 py-3 font-semibold">
                <Link to={`/${id.id}/warnings`} class="text-blue-500 text-sm font-semibold flex items-center  focus:outline-none">
                    Warnings
                </Link>
            </div>
        </aside>
    )
}