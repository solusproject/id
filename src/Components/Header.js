import { supabase } from '../supabaseClient'

export const Header = (email) => {
    return (
        <header class="bg-white shadow-sm w-screen fixed z-10  top-0 ">
            <div class="max-auto  px-8 py-2 bg-white">
                <div class="flex justify-between">
                    <a href="/">
                        <div class="logo flex items-center space-x-4 mr-10">
                            <img class="h-8" src="https://solusproject.org/assets/logo.svg"
                                alt=""></img>

                            <h1 class="text-gray-600 text-2xl">SolusID</h1>
                        </div>
                    </a>
                    <div class="float-right">
                        {email.email &&
                            <a class="flex items-center bg-blue-500 justify-center w-8 h-8 mx-2 p-5 text-white rounded-lg w-auto"
                                href="/me">{email.email.split("@")[0] || "Sign in"}</a>
                            ||
                            <a class="flex items-center bg-blue-500 justify-center w-8 h-8 mx-2 p-5 text-white rounded-lg w-auto"
                                href="/me">{email.email || "Sign in"}</a>
                        }
                    </div>
                </div>
            </div>
        </header>
    )
}