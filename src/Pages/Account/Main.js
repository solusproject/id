import { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { supabase } from '../../supabaseClient'
import { Header } from '../../Components/Header'
import { Sidebar } from '../../Components/Sidebar'

const Account = ({ session }) => {
  const { id } = useParams()
  const [email, setEmail] = useState(id ?? supabase.auth.user().email)
  const [loading, setLoading] = useState(true)
  const [username, setUsername] = useState(null)
  const [age, setDOB] = useState(null)
  const [bio, setBiography] = useState(null)

  /* Socials */
  const [discord, setDiscord] = useState(null)
  const [reddit, setReddit] = useState(null)
  const [youtube, setYouTube] = useState(null)
  const [facebook, setFacebook] = useState(null)
  const [instagram, setInstagram] = useState(null)
  const [tiktok, setTikTok] = useState(null)

  useEffect(() => {
    getProfile(email)
  }, [session])

  const getProfile = async (email) => {
    try {
      setLoading(true)

      let { data, error, status } = await supabase
        .from('user')
        .select(`*`)
        .ilike('email', email)
        .single()

      if (status == 406) {
        alert("User not found!")
        setUsername("User Not Found")
        setDOB("")
        setBiography("")
        setDiscord("")
        setReddit("")
        setYouTube("")
        setFacebook("")
        setInstagram("")
        setTikTok("")
      }

      if (error && status !== 406) {
        throw error
      }

      if (data) {
        setUsername(data.username)
        setDOB(data.age)
        setBiography(data.bio)
        setDiscord(data.discord)
        setReddit(data.reddit)
        setYouTube(data.youtube)
        setFacebook(data.facebook)
        setInstagram(data.instagram)
        setTikTok(data.tiktok)
      }
    } catch (error) {
      alert(error.details)
    } finally {
      setLoading(false)
    }
  }

  const updateProfile = async (e) => {
    e.preventDefault()

    try {
      setLoading(true)
      const user = supabase.auth.user()

      const updates = {
        id: user.id,
        email,
        username,
        age,
        bio,
        /* Socials */
        discord,
        instagram,
        facebook,
        tiktok,
        reddit,
        youtube
      }

      let { error, status } = await supabase.from('user').upsert(updates, {
        returning: 'minimal',
      })

      if (error) {
        alert(error.details)
      }

    } catch (error) {
      alert(error)

    } finally {
      setLoading(false)
    }
  }

  return (
    <div class="my-12">
      <Header email={supabase.auth.user().email}></Header>
      <div>
        <Sidebar id={email}></Sidebar>
        <div class="absolute px-4 pr-10 w-4/5">
          <div class="bg-white mt-5 w-4/5 float-left">

            {loading ? (
              'Loading ...'
            ) : (
              <form onSubmit={updateProfile} className="form-widget">
                {email == supabase.auth.user().email &&
                  <div>
                    <h2 class="text-gray-900 text-lg mb-1 font-medium title-font">Your Profile</h2>
                    <p class="leading-relaxed mb-5 text-gray-600">All information here is public and optional. By entering information, you agree that all information entered is public domain, up-to-date, and correct. If false information is entered, you will be banned from SolusID.</p>
                  </div> ||
                  <div>
                    <h2 class="text-gray-900 text-lg mb-1 font-medium title-font">{email.split("@")[0]}'s Profile</h2>
                  </div>
                }

                <div className="relative mb-4">
                  <label htmlFor="email" class="leading-7 text-sm text-gray-600">User Email</label>
                  <input
                    id="email"
                    class="w-full bg-white rounded border border-gray-300 focus:border-blue-500 focus:ring-2 focus:ring-blue-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out"
                    type="email"
                    value={email}
                    disabled={true}
                  />
                </div>
                <div className="relative mb-4">
                  <label htmlFor="username" class="leading-7 text-sm text-gray-600">Username</label>
                  <input
                    id="username"
                    class="w-full bg-white rounded border border-gray-300 focus:border-blue-500 focus:ring-2 focus:ring-blue-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out"
                    type="text"
                    value={username || ''}
                    onChange={(e) => setUsername(e.target.value)}
                    disabled={email != supabase.auth.user().email}
                  />
                </div>
                <div className="relative mb-4">
                  <label htmlFor="age" class="leading-7 text-sm text-gray-600">Date of Birth</label>
                  <input
                    id="age"
                    class="w-full bg-white rounded border border-gray-300 focus:border-blue-500 focus:ring-2 focus:ring-blue-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out"
                    type="date"
                    value={age || ''}
                    onChange={(e) => setDOB(e.target.value)}
                    disabled={email != supabase.auth.user().email}
                  />
                </div>
                <div className="relative mb-4">
                  <label htmlFor="bio" class="leading-7 text-sm text-gray-600">Biography</label>
                  <textarea
                    id="bio"
                    class="w-full bg-white rounded border border-gray-300 focus:border-blue-500 focus:ring-2 focus:ring-blue-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out"
                    type="text"
                    value={bio || ''}
                    cols="40"
                    rows="5"
                    onChange={(e) => setBiography(e.target.value)}
                    disabled={email != supabase.auth.user().email}
                  />
                </div>

                <hr></hr>
                <h2 class="text-gray-900 text-lg mb-1 font-medium title-font">Social Media Links</h2>
                <p>All of the below information is public. Please use direct URLs or IDs whenever possible.</p>
                <br></br>
                <div className="relative mb-4">
                  <label htmlFor="instagram" class="leading-7 text-sm text-gray-600">Instagram</label>
                  <input
                    id="instagram"
                    class="w-full bg-white rounded border border-gray-300 focus:border-blue-500 focus:ring-2 focus:ring-blue-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out"
                    type="url"
                    value={instagram || ''}
                    onChange={(e) => setInstagram(e.target.value)}
                    disabled={email != supabase.auth.user().email}
                  />
                </div>
                <div className="relative mb-4">
                  <label htmlFor="facebook" class="leading-7 text-sm text-gray-600">Facebook</label>
                  <input
                    id="facebook"
                    class="w-full bg-white rounded border border-gray-300 focus:border-blue-500 focus:ring-2 focus:ring-blue-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out"
                    type="url"
                    value={facebook || ''}
                    onChange={(e) => setFacebook(e.target.value)}
                    disabled={email != supabase.auth.user().email}
                  />
                </div>
                <div className="relative mb-4">
                  <label htmlFor="tiktok" class="leading-7 text-sm text-gray-600">TikTok</label>
                  <input
                    id="tiktok"
                    class="w-full bg-white rounded border border-gray-300 focus:border-blue-500 focus:ring-2 focus:ring-blue-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out"
                    type="url"
                    value={tiktok || ''}
                    onChange={(e) => setTikTok(e.target.value)}
                    disabled={email != supabase.auth.user().email}
                  />
                </div>
                <div className="relative mb-4">
                  <label htmlFor="youtube" class="leading-7 text-sm text-gray-600">YouTube</label>
                  <input
                    id="youtube"
                    class="w-full bg-white rounded border border-gray-300 focus:border-blue-500 focus:ring-2 focus:ring-blue-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out"
                    type="url"
                    value={youtube || ''}
                    onChange={(e) => setYouTube(e.target.value)}
                    disabled={email != supabase.auth.user().email}
                  />
                </div>
                <div className="relative mb-4">
                  <label htmlFor="reddit" class="leading-7 text-sm text-gray-600">Reddit</label>
                  <input
                    id="reddit"
                    class="w-full bg-white rounded border border-gray-300 focus:border-blue-500 focus:ring-2 focus:ring-blue-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out"
                    type="url"
                    value={reddit || ''}
                    onChange={(e) => setReddit(e.target.value)}
                    disabled={email != supabase.auth.user().email}
                  />
                </div>
                <div className="relative mb-4">
                  <label htmlFor="discord" class="leading-7 text-sm text-gray-600">Discord ID</label>
                  <input
                    id="discord"
                    class="w-full bg-white rounded border border-gray-300 focus:border-blue-500 focus:ring-2 focus:ring-blue-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out"
                    type="number"
                    value={discord || ''}
                    onChange={(e) => setDiscord(e.target.value)}
                    disabled={email != supabase.auth.user().email}
                  />
                </div>
                {email == supabase.auth.user().email &&
                  <div className="relative mb-4">
                    <button class="text-white bg-blue-500 border-0 py-2 px-6 focus:outline-none hover:bg-blue-600 rounded text-lg" disabled={loading}>
                      Update profile
                    </button>
                  </div>
                }
              </form>
            )}

          </div>
        </div>
      </div>
    </div>
  )
}

export default Account