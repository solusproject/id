import { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { Header } from '../../Components/Header'
import { Sidebar } from '../../Components/Sidebar'
import { supabase } from '../../supabaseClient'

const getPosts = async (email, setPosts) => {
    setPosts([])

    const { data: thisUser } = await supabase
        .from('user')
        .select('username')
        .ilike('email', email)
        .single()

    const { data: posts } = await supabase
        .from('posts')
        .select('from, content')
        .ilike('to', thisUser.username)

    if (posts) {
        for (const i of posts) {
            setPosts(postsFormatted => [...postsFormatted, (
                <li>{i.from}: {i.content}</li>
            )]);
        }
    } else {
        setPosts(postsFormatted => [...postsFormatted, (
            <li>System: Nobody has commented on this person's wall</li>
        )]);
    }
}

const Wall = () => {
    const { id } = useParams()
    const email = id ?? supabase.auth.user().email

    const [postsFormatted, setPosts] = useState([]);
    const [comment, setComment] = useState(null)

    useEffect(() => { getPosts(email, setPosts) }, [email])

    const submitComment = async (e) => {
        e.preventDefault()
        setComment(null)

        const { data: toUser, error: error3 } = await supabase
            .from('user')
            .select('username')
            .ilike('email', email)
            .single()

        const { data: fromUser, error: error2 } = await supabase
            .from('user')
            .select('username')
            .ilike('email', supabase.auth.user().email)
            .single()

        const { data, error, status } = await supabase
            .from('posts')
            .insert([
                { content: comment, to: toUser.username, from: fromUser.username },
            ])

        if (error) {
            alert(error.message)
        } else if (error2) {
            alert(error2.message)
        } else if (error3) {
            alert(error3.message)
        }
    }

    return (
        <div>
            <Header email={supabase.auth.user().email}></Header>
            <div class="my-12">
                <Sidebar id={email}></Sidebar>
                <div class="absolute px-4 pr-10 w-4/5">
                    <div class="bg-white ml-64  mt-5 float-left">
                        {email == supabase.auth.user().email &&
                            <div>
                                <h2 class="text-gray-900 text-lg mb-1 font-medium title-font">Your Profile</h2>
                                <p class="leading-relaxed mb-5 text-gray-600">All information here is public and optional. By entering information, you agree that all information entered is public domain.</p>
                            </div> ||
                            <div>
                                <h2 class="text-gray-900 text-lg mb-1 font-medium title-font">{email.split("@")[0]}'s Profile</h2>
                            </div>
                        }
                        <ul>
                            {postsFormatted}
                        </ul>
                        <hr className="my-10"></hr>
                        <form onSubmit={submitComment} className="form-widget">
                            <label htmlFor="email" class="leading-7 text-sm text-gray-600">Make a post...</label>
                            <input
                                id="comment"
                                class="w-full bg-white rounded border border-gray-300 focus:border-blue-500 focus:ring-2 focus:ring-blue-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out"
                                type="text"
                                value={comment || ''}
                                onChange={(e) => setComment(e.target.value)}
                            />
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Wall