import { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { Header } from '../../Components/Header'
import { Sidebar } from '../../Components/Sidebar'
import { supabase } from '../../supabaseClient'

const getPosts = async (email, setPosts) => {
    setPosts([])

    const { data: thisUser } = await supabase
        .from('user')
        .select('username')
        .ilike('email', email)
        .single()

    const { data: posts } = await supabase
        .from('reports')
        .select('reason')
        .ilike('to', thisUser.username)

    if (posts) {
        for (const i of posts) {
            setPosts(postsFormatted => [...postsFormatted, (
                <li>{i.content}</li>
            )]);
        }
    } else {
        setPosts(postsFormatted => [...postsFormatted, (
            <li>No Solus-Authorized Reports have been made against this user at this time.</li>
        )]);
    }
}

const Warnings = () => {
    const { id } = useParams()
    const email = id ?? supabase.auth.user().email
    const [postsFormatted, setPosts] = useState([]);
    useEffect(() => { getPosts(email, setPosts) }, [email])

    return (
        <div>
            <Header email={supabase.auth.user().email}></Header>
            <div class="my-12">
                <Sidebar id={email}></Sidebar>
                <div class="absolute px-4 pr-10 w-4/5">
                    <div class="bg-white ml-64  mt-5 float-left">
                        {email == supabase.auth.user().email &&
                            <div>
                                <h2 class="text-gray-900 text-lg mb-1 font-medium title-font">Your Profile</h2>
                                <p class="leading-relaxed mb-5 text-gray-600">All information here is public and optional. By entering information, you agree that all information entered is public domain.</p>
                            </div> ||
                            <div>
                                <h2 class="text-gray-900 text-lg mb-1 font-medium title-font">{email.split("@")[0]}'s Profile</h2>
                            </div>
                        }
                        <ul>
                            {postsFormatted}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Warnings