import { useState } from 'react'
import { Header } from '../Components/Header'
import { Link } from "react-router-dom";

const Search = () => {
    const [query, setQuery] = useState('')
    return (
        <div class='flex items-center justify-center min-h-screen py-4'>
            <Header></Header>
            <div class="flex items-center w-1/2 mx-auto bg-white rounded-lg">
                <div class="w-full">
                    <input type="search" class="w-full px-4 py-2 text-gray-800 w-full rounded-lg outline outline-offset-2 outline-2 outline-gray-200"
                        placeholder="Search via any email..." onChange={(e) => setQuery(e.target.value)}></input>
                </div>
                <div>
                    <Link to={`/${query}`} class="flex items-center bg-blue-500 justify-center w-12 h-12 mx-2 text-white rounded-lg hover:bg-blue-600'">
                        <svg class="w-5 h-5" fill="none" stroke="currentColor" viewBox="0 0 24 24"
                            xmlns="http://www.w3.org/2000/svg">
                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
                                d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path>
                        </svg>
                    </Link>
                </div>
            </div>
        </div >
    )
}

export default Search