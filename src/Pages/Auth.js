import { useState } from 'react'
import { supabase } from '../supabaseClient'
import { Header } from '../Components/Header'

export default function Auth() {
    const [loading, setLoading] = useState(false)
    const [email, setEmail] = useState('')

    const handleLogin = async (e) => {
        e.preventDefault()

        try {
            setLoading(true)
            const { error } = await supabase.auth.signIn({ email })
            if (error) throw error
            alert('Check your email for the login link!')
        } catch (error) {
            alert(error.error_description || error.message)
        } finally {
            setLoading(false)
        }
    }

    return (
        <section class="text-gray-600 body-font relative">
            <Header></Header>
            <div class="container px-5 py-24 mx-auto flex sm:flex-nowrap flex-wrap">
                <div
                    class="lg:w-2/3 md:w-1/2 bg-gray-300 rounded-lg overflow-hidden sm:mr-10 p-10 flex items-end justify-start relative">
                    <img src="world.svg"></img>

                </div>
                {loading ? (
                    'Sending your sign-in link...'
                ) : (
                    <form onSubmit={handleLogin} class="lg:w-1/3 md:w-1/2 bg-white flex flex-col md:ml-auto w-full md:py-8 mt-8 md:mt-0">
                        <h2 class="text-gray-900 text-lg mb-1 font-medium title-font">Log in</h2>
                        <p class="leading-relaxed mb-5 text-gray-600">Log in to continue to SolusID</p>
                        <div class="relative mb-4">
                            <label htmlFor="email" class="leading-7 text-sm text-gray-600">Email</label>
                            <input
                                id="email"
                                class="w-full bg-white rounded border border-gray-300 focus:border-blue-500 focus:ring-2 focus:ring-blue-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out"
                                type="email"
                                placeholder="Your email"
                                value={email}
                                onChange={(e) => setEmail(e.target.value)}
                            />
                        </div>
                        <button class="text-white bg-blue-500 border-0 py-2 px-6 focus:outline-none hover:bg-blue-600 rounded text-lg" aria-live="polite">
                            Sign in
                        </button>
                    </form>
                )}
            </div>
        </section >
    )
}