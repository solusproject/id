import Auth from './Pages/Auth'
import Account from './Pages/Account/Main'

export default function App(session) {
  return (
    <div>
      {!session.session ? <Auth /> : <Account key={session.session.user.id} session={session.session} />}
    </div>
  )
}