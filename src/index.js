import React from 'react';
import ReactDOM from 'react-dom/client';
import { supabase } from './supabaseClient'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import Search from './Pages/Search'
import App from './App'
import Account from './Pages/Account/Main'
import Wall from './Pages/Account/Wall'
import Warnings from './Pages/Account/Warnings'

const root = ReactDOM.createRoot(document.body);

let session = supabase.auth.session()
root.render(
  <React.StrictMode>
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Search />} />
        <Route path="/:id/wall" element={<Wall />} />
        <Route path="/:id/warnings" element={<Warnings />} />
        <Route path="/:id" element={<Account session={session} />}>
        </Route>
        <Route path="/me" element={<App session={session} />} />
      </Routes>
    </BrowserRouter>
  </React.StrictMode>
);